# Balance

Escreva um programa de linha de comando para calcular o balanço da conta corrente de um
conjunto de clientes.

## Invocação

O programa deve receber dois argumentos na linha de comando:

1. Nome do arquivo de contas
2. Nome do arquivo de transações

Ex: `./exercicio contas.csv transacoes.csv`

### Formato de Entrada

Os arquivos de entrada estão em formato CSV, com campos delimitados por vírgula,
sem aspas, sem cabeçalho.

**CSV de Contas**

1. Id da conta (int)
2. Saldo inicial na conta, em centavos de real (int)

Ex: `345,14428`

> (Conta 345, saldo R$144,28)

**CSV de Transações**

1. Id da conta (int)
2. Valor da transação, em centavos de real (int)

Uma transação de valor positivo é um depósito na conta.
Uma transação de valor negativo é um débito na conta.

Ex: `345,­2000`

> (Débito de R$20 na conta 345)

Ex: `345,120000`

> (Depósito de R$1200,00 na conta 345)

### Cálculo do Saldo

O saldo de uma conta deve ser calculado a partir de seu saldo inicial, aplicando cada uma
das transações relacionadas a esta conta. Depósitos devem aumentar o saldo da conta e
débitos devem reduzir esse mesmo saldo, na medida do valor da transação.
Uma conta pode assumir um valor negativo e não existe limite inferior para o saldo da conta.
Contudo, cada transação de débito que termina deixando o saldo da conta negativo implica
uma multa de R$5, a ser descontada imediatamente. Esta multa se aplica independente da
conta se encontrar ou não com saldo negativo antes da transação, mas não se aplica se a
transação for um depósito.
As transações devem ser processadas na ordem em que são apresentadas no arquivo de
entrada.

### Formato de Saída

A saída deve ser impressa na tela (stdout) em formato CSV, com campos delimitados por
vírgula, sem aspas, sem cabeçalho.

1. Id do cliente (int)
2. Saldo final na conta, em centavos de real ()

Ex: `345,28856`

> (Saldo final de R$288,56 na conta 345)

## Observações

Seu código deve estar acompanhado de testes, mas não é necessário ter 100% de cobertura.

Você deve implementar tratamento para os erros mais óbvios, mas pode decidir como este tratamento será feito.

Iremos considerar especialmente a clareza da sua solução e o uso de boas práticas de ~~orientação a objetos~~ .

Como resposta, envie um repositório git contendo sua solução e um arquivo Readme com
instruções de como executar o código e os testes.

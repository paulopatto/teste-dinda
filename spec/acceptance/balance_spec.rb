require 'spec_helper'

describe "Balance" do

  let(:account_id) { 1234 }
  let(:balance) { 10000 }

  describe "#negative?" do
    context "when receive positive value" do
      let(:positive_value) { 1 }
      subject { negative?(positive_value) }

      it { expect(subject).to be_falsey }
    end

    context "when receive negative value" do
      let(:negative_value) { -1 }
      subject { negative?(negative_value) }

      it { expect(subject).to be_truthy }
    end
  end

  describe "#create_account" do
    subject { create_account(account_id, balance) }

    it "returns hash" do
      expect(subject).to be_kind_of(Hash)
    end

    it "returns hash key as account id" do
      expect(subject).to be_include(account_id.to_s)
    end

    it "returns hash value as account balance" do
      expect(subject[account_id.to_s]).to eq(balance)
    end

    it "add account in accounts collection" do
      subject
      expect(accounts).to be_include(account_id.to_s)
      expect(accounts[account_id.to_s]).to be(balance)
    end
  end

  describe "#transaction" do
    context "When receive invalid account" do
      it "returns 0" do
        expect(transaction("xxxx", balance)).to eq(0)
      end

      it "doesn't call change_balance" do
        expect(self).to_not receive(:change_balance)
        transaction("xxx", balance)
      end
    end

    context "When receive invalid value" do
      it "returns 0" do
        expect(transaction(account_id, -0.5)).to eq(0)
      end

      it "doesn't call change_balance" do
        expect(self).to_not receive(:change_balance)
        transaction(account_id, 0.1)
      end
    end

    context "When receive positive value (#credit)" do
      let(:credit_value) { 5000 }

      before do
        create_account(account_id, balance)
      end

      subject { self }

      it "calls credit function with account_id and credit_value" do
        expect(subject).to receive(:credit).with(account_id, credit_value)
        transaction(account_id, credit_value)
      end

      it "doesn't calls debit function" do
        expect(subject).to_not receive(:debit).with(any_args)
        transaction(account_id, credit_value)
      end

      it "after credit account balance upto new balance with credit value" do
        transaction(account_id, credit_value)
        expect(accounts[account_id.to_s]).to eq(balance + credit_value)
      end
    end

    context "When receive negative value (#debit)" do
      let(:debit_value) { -5000 }

      before do
        create_account(account_id, balance)
      end

      subject { self }

      it "doesn't calls credit function" do
        expect(subject).to_not receive(:credit)
        transaction(account_id, debit_value)
      end

      it "calls debit function" do
        expect(subject).to receive(:debit).with(account_id, debit_value)
        transaction(account_id, debit_value)
      end

      it "debit value in account balance" do
        new_balance = transaction(account_id, debit_value)
        expect(new_balance).to eq(balance + debit_value)
      end

      it "check new balance on accounts collection" do
        transaction(account_id, debit_value)
        expect(accounts[account_id.to_s]).to eq(balance + debit_value)
      end

      context "case debit value above account balance, apply fine" do
        let(:debit_value_above_balance) { -20000 }
        it "call #apply_fine" do
          expect(subject).to receive(:apply_fine).with(account_id)
          transaction(account_id, debit_value_above_balance)
        end

        it "new balance equals debit value with fine" do
          new_balance = transaction(account_id, debit_value_above_balance)
          expect(new_balance).to eq(balance + debit_value_above_balance + LIS_FINE)
        end

        it "check new balance with fine on accounts collection" do
          transaction(account_id, debit_value_above_balance)
          expect(accounts[account_id.to_s]).to eq(balance + debit_value_above_balance + LIS_FINE)
        end
      end
    end
  end

  # Não vai ficar bom por causa do sono já.
  # Deveria tratar as exceções aqui como valores floats no csv
  # ou ids de contas bizarros.
  # Mas vai que a conexão cai de novo...
  describe "#change_balance" do
    context "called when" do
      let(:debit_value) { -5000 }
      let(:credit_value) { 5000 }
      let(:fine_value) { 500 }

      before { create_account(account_id, balance) }

      subject { self }

      it "debit transaction" do
        expect(subject).to receive(:change_balance).with(account_id, debit_value).and_return(1)
        transaction(account_id, debit_value)
      end

      it "credit transaction" do
        expect(subject).to receive(:change_balance).with(account_id, credit_value)
        transaction(account_id, credit_value)
      end
    end
  end
end

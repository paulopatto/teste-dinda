LIS_FINE = -500

def accounts
  @accounts ||= {}
end

# Evita o monkey patch
def negative?(number)
  number < 0
end

def create_account(account_id, balance)
  unless accounts.include?(account_id.to_s)
    account = { account_id.to_s => balance }
    accounts.merge!(account)
    return account
  end
end

def valid_value?(value)
  value.kind_of?(Fixnum) or value.kind_of?(Bignum)
end

def account_exists?(account_id)
  !accounts[account_id.to_s].nil?
end

def transaction(account_id, value)
  # Verifica se é uma conta válida (existe)
  return 0 unless account_exists?(account_id)

  # Valida de a transação tem um valor válida
  return 0 unless valid_value?(value)

  negative?(value) ? debit(account_id, value) : credit(account_id, value)
end

def credit(account_id, value)
  change_balance(account_id, value)
end

def debit(account_id, value)
  apply_fine(account_id) if negative?(change_balance(account_id, value))

  accounts[account_id.to_s]
end

def apply_fine(account_id)
  change_balance(account_id, LIS_FINE)
end

def change_balance(account_id, value)
  accounts[account_id.to_s] += value
  accounts[account_id.to_s]
end

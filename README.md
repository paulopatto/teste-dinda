# Balanço de contas

Software de calcula o balanço de contas bancárias.
Este código foi feito em ruby usando mais o paradigma de funções como prova de conceito inicial.
Pode ser que isso mude em um futuro próximo... Se a internet não cair novamente.


## Requerimentos

- Ruby (de preferência 2.2.2)
- RSpec
- Arquivos csv de contas e transações conforme [PROPOSAL.md](PROPOSAL.md)

## Como executar o código

Para usar o script você deve passar dois argumentos na linha de comando:

- Nome/caminho do arquivo de contas
- Nome/caminho do arquivo de transações

Chame o progama com a linha a seguir:

`./exercicio contas.csv transacoes.csv`

> No caso de não passar os dois arquivo o script irá emitir a seguinte mensagem:
> Usage: `$ [ruby] exercicio ACCOUNT_FILE.csv TRANSACTIONS_FILE.csv`

## Testes

Para rodar os testes você deve ter a gem [RSpec](http://rspec.info) instalada, se você executo a instalação com `Bundler` ele já está na máquina.
Ai é só executar a seguinte linha de comando:

`rspec` ou `rspec spec/`
